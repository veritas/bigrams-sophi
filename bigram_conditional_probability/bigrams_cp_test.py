import nltk
from nltk.tokenize import word_tokenize
from nltk.corpus import brown


def learn_cp_from_brown():
    brown_bigrams = nltk.bigrams(brown.words())
    return nltk.ConditionalFreqDist((w0.lower(), w1.lower()) for w0, w1 in brown_bigrams)


class CondProb:

    def __init__(self):
        self.cfd = learn_cp_from_brown()

    '''
    This method allow you to update the ConditionalFreqDist with more sentences you want it to learn.
    :param sent: (list, str) The sentence can be a list of items, or a string;
    
    '''
    def update_cfd(self, sent):
        if type(sent) is str:
            sent = word_tokenize(sent)
        elif type(sent) is not list:
            raise TypeError("sent for update needs to be either list of words, or a string of sentence")
        if len(sent) < 2:
            return
        pairs = []
        for i in range(1, len(sent)):
            pairs.append((sent[i - 1].lower(), sent[i].lower()))
        self.cfd.update(nltk.ConditionalFreqDist(pairs))

    '''
        This method allow you to query the conditional probability for second_word following first_word.
        :param first_word: (str) The leading word;
        :param first_word: (str) The leading word;
        
        :return (float) the probability of this word pair

        '''
    def query_probability(self, first_word, second_word):
        first_word, second_word = first_word.lower(), second_word.lower()
        if len(self.cfd[first_word]) == 0:
            # did not bother to config log
            print("LOG: Never see this word '{}' leading anything in the history.".format(first_word))
        return self.cfd[first_word].freq(second_word)

    '''
        This method allow you to list all the learnt potential following word for the given word, with probabilities.
        :param word: (str) The leading word;
        
        :return (list [(word, probability)]) the potential words, and each's corresponding probablity

        '''
    def what_is_after(self, word):
        word = word.lower()
        return [(x[0], self.cfd[word].freq(x[0])) for x in self.cfd[word].items()]


if __name__ == "__main__":
    cp = CondProb()
    print("Possible words and prob following the leading word '{}' are: {}".format("fox", cp.what_is_after("fox")))
    print("Possible words and prob following the leading word '{}' are: {}".format("Jack", cp.what_is_after("Jack")))

    print("The probability of '{}' after '{}' is {}".format('a', '.', cp.query_probability('a', '.')))
    # non-show word
    print("The probability of '{}' after '{}' is {}".format('fsafdasfs', '.', cp.query_probability('fasdfsadf', '.')))

