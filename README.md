# bigrams-sophi

# # Explanation
So I was basically using the NLTK to do a rough demo of how to just basically count the frequency of one word appears after another, and memorize this for future prediction.
No fancy technologies like HMM or RNN is involved.

The idea is quite simple: break the sentence of training corpus into bigrams, meaning all the possibly continuous two words (or word with symbol); And then use a built in tool to calculate the frequency distribution through all the bigrams, which will make it possible for us to predict the future input with a potential following word.

Each of the interfaces (methods) has its document, so I'm not going to explain too much here. But let me cover some of my tech decisions:
1. I treat all pairs with lower case, means the algorithm is not case sensitive, because we can see words appearing in different case format but means the same word pair, and the probablity of different cases but same words should be accumulated.
2. I did not clean out any symbols, or stop words. Because we care about the position of tokens (words, symbols), and symbols for some certain words can be very important and frequent following element. This is also very useful to detect the end of sentence inputing stream.


Weakness:
1. All data are loaded into memory in my training process, but actually this can be serialized. By just using my `CondProb.update()` method, this can be achieved.
2. For more dirty corpus as training materials, we will need to use more pre-process, like removing unicode symbols (emoji,etc.), and meaningless repeat symbols, etc.
3. This algorithm itself is quite limited, because it cannot count in any information about semantic relations other than the leading words. We can use more complex model/method to calculate the probability more accurately, not only based on what's leading, but also based on where this pair is appearing in the sentence, or what words are before this pair.
For example, we can use POS tagging to determine what part is this leading word belonging, so we'd be able to more finely know the potential following words. Like for 'book', if it's a `verb`, you might want to guess 'a hotel' to follow; but if it's a `noun`, you might want to guess 'is interesting' to follow.
Also using RNN you will be able to remember more information about things appearing before the pair to make prediction.

